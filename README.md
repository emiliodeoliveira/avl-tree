## Introdução

Arvores balanceadas são muito eficientes para encontrar itens em armazenados em listas sortidas. As árvores balanceadas, também conhecidas como AVLs, se caracterizam por sempre manterem a diferença de suas subárvores em apenas uma unidade. A sigla AVL vem em homenagem aos seus inventores Adelson-Velskii and Landis.

O fator de balanceamento da AVL é a diferença da altura da subárvore da esquerda com a da direita. Esse fator se e somente pode ser -1 ou +1.

![Untitled](docs/1.png)

![Untitled](docs/2.png)

## Comportamento

o tempo das operações básicas são proporcionais aos numeros de níveis.