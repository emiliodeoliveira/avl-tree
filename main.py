from AVLSearchTree import AVLSearchTree

avlTree = AVLSearchTree()
root = None
nums = [11, 32, 15, 9, 54, 36, 71, 23, 38, 3, 24]
for num in nums:
    root = avlTree.insertNode(root, num)
avlTree.printTree(root, "", True)
avlTree.delete(root, 23)
avlTree.delete(root, 15)
avlTree.printTree(root, "", True)

print("AVL Tree Menu")
option = 0


def printMenu():
    print("***********************************")
    print("Enter the option:")
    print(" --- 1: Insert")
    print(" --- 2: Delete")
    print(" --- 4: Show the tree")
    print(" --- 5: Pre order display")
    print(" --- 6: Post order display")
    print(" --- 7: In order display")
    print(" --- 8: Display tree height")
    print(" --- 9: Show leaf number")
    print(" --- 10: Count nodes")
    print(" --- 11: Exit")
    print("***********************************")


while option != 11:
    printMenu()
    option = int(input("-> "))
    if option == 1:
        x = int(input(" Input the value to insert -> "))
        avlTree.insertNode(root, x)
    elif option == 2:
        x = int(input(" Input the value for deletion -> "))
        avlTree.delete(root, x)
    elif option == 4:
        avlTree.printTree(root, "", True)
    elif option == 5:
        print(" Pre order: ", end="")
        avlTree.preOrder(root)
    elif option == 6:
        print(" Post order: ", end="")
        avlTree.posOrder(root)
    elif option == 7:
        print(" In order: ", end="")
        avlTree.inOrder(root)
    elif option == 8:
        print("Tree height: %d" % (avlTree.getTreeHeight(root)))
    elif option == 9:
        print("Leaf number: %d" % (avlTree.getLeafs(root)))
    elif option == 10:
        print("Tree nodes: %d" % (avlTree.countNodes(root)))
    elif option == 11:
        break
