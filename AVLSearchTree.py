from TreeNode import TreeNode
import sys


class AVLSearchTree(object):
    def insertNode(self, root, value):

        if not root:
            return TreeNode(value)
        elif value < root.value:
            root.left = self.insertNode(root.left, value)
        else:
            root.right = self.insertNode(root.right, value)
        # Returns the bigger height from two sides
        root.height = 1 + max(self.getHeight(root.left), self.getHeight(root.left))

        balance = self.getBalance(root)
        # This block updates the balance factor value and balance the tree
        if balance > 1:
            if value < root.left.value:
                return self.rightRotation(root)
            else:
                root.left = self.leftRotation(root.left)
                return self.rightRotation(root)

        if balance < -1:
            if value > root.right.value:
                return self.leftRotation(root)
            else:
                root.right = self.rightRotation(root.right)
                return self.leftRotation(root)
        return root

    def getHeight(self, root):
        if not root:
            return 0
        return root.height

    def delete(self, root, value):

        # Finds the node to be deleted
        if not root:
            print("A node with this value was not found!")
            return root
        elif value < root.value:
            root.left = self.delete(root.left, value)
        elif value > root.value:
            root.right = self.delete(root.right, value)
        else:
            if root.left is None:
                temp = root.right
                root = None
                return temp

            elif root.right is None:
                temp = root.left
                root = None
                return temp

            temp = self.getMinValue(root.right)
            root.value = temp.value
            root.right = self.delete(root.right, temp.value)
        if root is None:
            return root

        root.height = 1 + max(self.getHeight(root.left), self.getHeight(root.right))
        balanceFactor = self.getBalance(root)

        if balanceFactor > 1:
            if self.getBalance(root.left) > 0:
                return self.rightRotation(root)
            else:
                root.left = self.leftRotation(root.left)
                return self.rightRotation(root)
        if balanceFactor < -1:
            if self.getBalance(root.right) <= 0:
                return self.leftRotation(root)
            else:
                root.right = self.rightRotation(root.right)
                return self.leftRotation(root)
        return root

    def leftRotation(self, val):
        aux = val.right
        tempVal = aux.left

        aux.left = val
        val.right = tempVal

        val.height = 1 + max(self.getHeight(val.left), self.getHeight(val.right))
        aux.height = 1 + max(self.getHeight(aux.left), self.getHeight(aux.right))

        return aux

    def countNodes(self, value):
        if value == None:
            return 0
        else:
            return 1 + self.countNodes(value.left) + self.countNodes(value.right)

    def rightRotation(self, val):
        aux = val.left
        tempVal = aux.right

        aux.right = val
        val.left = tempVal

        val.height = 1 + max(self.getHeight(val.left), self.getHeight(val.right))
        aux.height = 1 + max(self.getHeight(aux.left), self.getHeight(aux.right))

        return aux

    def getBalance(self, root):
        if not root:
            return 0

        return self.getHeight(root.left) - self.getHeight(root.right)

    def getMinValue(self, root):
        if root is None or root.left is None:
            return root
        return self.getMinValue(root.left)

    def preOrder(self, root):
        if not root:
            return

        print("{0} ".format(root.value), end="")
        self.preOrder(root.left)
        self.preOrder(root.right)

    def inOrder(self, root):
        if not root:
            return

        self.inOrder(root.left)
        print("{0} ".format(root.value), end="")
        self.inOrder(root.right)

    def posOrder(self, root):
        if not root:
            return

        self.inOrder(root.left)
        self.inOrder(root.right)
        print("{0} ".format(root.value), end="")

    def printTree(self, currPtr, indent, last):
        if currPtr != None:
            sys.stdout.write(indent)
            if last:
                sys.stdout.write("R----")
                indent += "     "
            else:
                sys.stdout.write("L----")
                indent += "|    "
            print(currPtr.value)
            self.printTree(currPtr.left, indent, False)
            self.printTree(currPtr.right, indent, True)

    def minimumValue(self, root):
        value = root
        aux = None
        while value is not None:
            aux = value
            value = value.left
        return aux

    def maxValue(self, root):
        value = root
        aux = None
        while value is not None:
            aux = value
            value = value.right
        return aux

    def getTreeHeight(self, value):
        if value is None or value.left is None and value.right is None:
            return 0
        else:
            if self.getTreeHeight(value.left) > self.getTreeHeight(value.right):
                return 1 + self.getTreeHeight(value.left)
            else:
                return 1 + self.getTreeHeight(value.right)

    def getLeafs(self, value):
        if value is None:
            return 0
        if value.left is None and value.right is None:
            return 1
        return self.getLeafs(value.left) + self.getLeafs(value.right)
